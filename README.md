Database Schema:

CREATE DATABASE `petasense`
USE `petasense`;
CREATE TABLE `petasense`.`machines` (
    `machine_id` INT NOT NULL AUTO_INCREMENT,
    `machine_name` char(32) NOT NULL,
    `condition` INT NOT NULL, 
    `last_measurement_time` datetime DEFAULT '0000-00-00 00:00:00'
    PRIMARY KEY (`machine_id`)
);

CREATE TABLE `petasense`.`parts` (
    `part_id` INT NOT NULL AUTO_INCREMENT,
    `part_name` char(32) NOT NULL,
    PRIMARY KEY (`part_id`)
);

CREATE TABLE `petasense`.`parameters` (
    `parameter_id` INT NOT NULL AUTO_INCREMENT,
    `parameter_name` char(32) NOT NULL,
    PRIMARY KEY (`parameter_id`)
);

CREATE TABLE `petasense`.`sensors` (
    `sensor_id` INT NOT NULL AUTO_INCREMENT,
    `machine_id` INT NOT NULL,
    `part_id` INT NOT NULL,
    `parameter_id` INT NOT NULL,
    PRIMARY KEY (`sensor_id`),
    FOREIGN KEY (machine_id) REFERENCES machines(machine_id),
    FOREIGN KEY (part_id) REFERENCES parts(part_id),
    FOREIGN KEY (parameter_id) REFERENCES parameters(parameter_id)
); 

CREATE TABLE `petasense`.`transmitter_data` (
    `reading_id` INT NOT NULL AUTO_INCREMENT,
    `transmitter_id` INT NOT NULL,
    `sensor_id` INT NOT NULL,
    `port` INT NOT NULL,
    `value` FLOAT(12, 2) NOT NULL,
    `datetime` datetime DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`reading_id`),
    FOREIGN KEY (sensor_id) REFERENCES sensors(sensor_id)
);

Sample data population:

INSERT INTO `petasense`.`machines` (`machine_name`, `condition`, `last_measurement_time`) 
VALUES ('machine1', 5, NOW()), ('machine2', 3, NOW());

INSERT INTO `petasense`.`parts` (`part_name`)
VALUES ('motor'), ('shaft'), ('bearings'), ('pump');

INSERT INTO `petasense`.`parameters` (`parameter_name`)
VALUES ('vibration'), ('temperature'), ('pressure'), ('current'), ('ultrasound');

INSERT INTO `petasense`.`sensors` (`machine_id`, `parameter_id`, `part_id`)
VALUES (1, 1, 3), (1, 2, 3), (1, 5, 3), (1, 3, 4), (1, 4, 1),  (2, 1, 3), (2, 2, 3), (2, 5, 3), (2, 3, 4), (2, 4, 1); 

DROP PROCEDURE IF EXISTS sample_readings;
DELIMITER //  
CREATE PROCEDURE sample_readings()
BEGIN
    DECLARE count INT DEFAULT 0;
    WHILE count < 3600 DO
        INSERT INTO `petasense`.`transmitter_data` (`transmitter_id`, `sensor_id`, `port`, `value`, `datetime`)
        SELECT FLOOR(RAND() * 9) + 1, `sensor_id`, FLOOR(RAND() * 4) + 1, RAND(), DATE_ADD(DATE_SUB(NOW(), INTERVAL 5 MONTH), INTERVAL count HOUR)
        FROM `petasense`.`sensors`;
        SET count = count + 1;
    END WHILE;
END;
//
DELIMITER ;
CALL sample_readings();

Assumptions:
1. Machine's condition is periodically updated in the database via a cron job based on the average value calculated for all the parameters in that period. Condition is maintained on a scale of 1-5 and color coding can be assinged based on this.
2. Machine's last_measurement_time is updated on every reading. We can write a trigger or update via query whenever a reading is being inserted into trasmitter_data
