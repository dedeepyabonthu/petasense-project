import json
from flask import Flask, render_template
from flaskext.mysql import MySQL

from config import CONFIG

mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = CONFIG['database']['user']
app.config['MYSQL_DATABASE_DB'] = CONFIG['database']['database_name']
app.config['MYSQL_DATABASE_HOST'] = CONFIG['database']['host']
app.config['MYSQL_DATABASE_PASSWORD'] = CONFIG['database']['password']
mysql.init_app(app)


@app.route('/')
def status():
    return 'Alive'


@app.route('/list_machines/')
def list_machines():
    cursor = mysql.connect().cursor()
    data = list_machines_fetcher(cursor)['response']
    print data
    return render_template('machines_list.html', data=data)
#    return format_response(data)


@app.route('/machine_details/<machine_id>')
def machine_details(machine_id):
    cursor = mysql.connect().cursor()
    data = machine_details_fetcher(cursor, machine_id)['response']
    return format_response(data)


def list_machines_fetcher(cursor):
    cursor.execute('''
SELECT machine_name,
       `condition`,
       last_measurement_time
  FROM machines''')

    response = []
    if not cursor.rowcount:
        return {'response': response}

    for machine_name, condition, last_measurement_time in cursor.fetchall():
        response.append({
            'machine_name': str(machine_name),
            'condition': condition,
            'last_measurement_time': last_measurement_time.strftime(
                "%Y-%m-%d %H:%M:%S")
        })

    return {'response': response}


def machine_details_fetcher(cursor, machine_id):
    cursor.execute('''
SELECT value,
       parameter_id,
       datetime
  FROM transmitter_data
  JOIN sensors
 USING (sensor_id)
  JOIN machines
 USING (machine_id)
 WHERE machine_id = %s''')

    if not cursor.rowcount:
        return {'response': []}

    response = {}
    for parameter_id, value, date_time in cursor.fetchall():
        response.setdefault(parameter_id, [])
        response[parameter_id].append({
            'value': value,
            'datetime': date_time.strftime("%Y-%m-%d %H:%M:%S")
        })

    return {'response': response}


def format_response(results):
    response = app.response_class(
        response=json.dumps(results),
        status=200,
        mimetype='application/json'
    )
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


if __name__ == '__main__':
    app.run()
